
function validateFormOnSubmit() {
	var nameValue = document.getElementById('Name').value;
	var emailValue = document.getElementById('Email').value;
	var phoneValue = document.getElementById('Phone').value;
	
	var reason = "";
	
	if (emailValue != "" && phoneValue != "") {
		reason += CheckEmail(emailValue);
		reason += CheckPhone(phoneValue);
	}
	if (emailValue != "" && phoneValue =="") {
		reason += CheckEmail(emailValue);
	} 
	if (phoneValue != "" && emailValue =="") {
		reason += CheckPhone(phoneValue);
	}
	if (phoneValue =="" && emailValue =="") {
		reason="Please Enter an Email or Phone number \n";
	}
	
	reason += CheckName(nameValue);
	
	if (document.getElementById("otherReason").style.display === 'block') {
		if (document.getElementById("otherText").value !="") {
			reason +="Please enter 'Other' reason for contact \n";
		}
	}
		var checkBoxes = document.getElementsByClassName('contactDate');
		var isChecked = false;
    for (var i = 0; i < checkBoxes.length; i++) {
        if ( checkBoxes[i].checked ) {
            isChecked = true;
        };
    };
	if (!isChecked) {
		reason += "Please choose a date to best contact you \n"}
		
		if (reason != "") {
			alert("Some fields need correction:\n" + reason);
			return false;
		}
		
		return true;
	}
	
	function CheckName(fld) {
		var error = "";
		if (fld==null || fld=="") {
			error = "Please enter your name \n";
		}
		return error;
	}
	function CheckEmail(fld) {
		var error="";
		if (fld == "") {
			error = "Please enter an email \n";
		}
		return error;
	}
	function CheckPhone(fld) {
		var error = "";  
		if (fld == "") {
			error = "Please enter a phone number  \n";
		} 
		return error;
	}


function CheckReason(val) {
	if(val==='Other')
	document.getElementById('otherReason').style.display='block';
	else
	document.getElementById('otherReason').style.display='none';	
}